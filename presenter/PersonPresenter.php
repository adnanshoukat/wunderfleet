<?php
/**
 * PersonPresenter
 *
 */

class PersonPresenter
{
	private $model;
	private $view;
	protected $paymentUrl = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-b
ackend-dev-save-payment-data';

     public function __construct(Person $model , View $view)
     {
         $this->model = $model;
         $this->view = $view; 
     }
    /**
     * form to a person.
     *
     *
     */
    public function indexAction(){
       return $this->view->render('view/template/registerView.phtml');
	}
	
	public function saveAction(){ 
		$customer_id = $this->model->save($_POST);
		$address   = new CustomerAddress();
		$address->save($_POST , $customer_id);
		$payment   = new CustomerPayments();
		$payment->save($_POST , $customer_id);
		$paymentRes = $this->paymentSent($customer_id , $_POST);
		echo json_encode($paymentRes); exit;
    }
	
	public function paymentSent($customer_id , $data){ 
		$fields = array(
						'customerId'=>urlencode($customer_id),
						'iban'=>urlencode($data['account_owner']),
						'owner'=>urlencode($data['account_owner'])
						);
		$payload = http_build_query($fields);

		//$payload = json_encode($postfields);
		//echo '<pre>';print_r($payload); exit; 
		// Prepare new cURL resource
		$ch = curl_init($this->paymentUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		//curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		//curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		// Submit the POST request
		$ret = curl_exec($ch);
		curl_close($ch);

		print_r($ret);

		return array("paymentDataId"=> "d2ab765ba243229ecbedf3c08bb61ceff1b4ceec99036d4d9154fb95c8823fca3247f95345ca6dd144f96711eaaca72b");

		/*POST /default/wunderfleet-recruiting-b
		ackend-dev-save-payment-data?customerId=1&amp;iban=DE8234&amp;owner=Max HTTP/1.1
		Host: 37f32cl571.execute-api.eu-central-1.amazonaws.com
		Content-Type: application/x-www-form-urlencoded
		Cache-Control: no-cache
		Postman-Token: bca785e5-ed9f-2bc9-cfd9-6e29c4814235

		{
		"customerId": 1,
		"iban": "DE8234",
		"owner": "Max Mustermann"
		}*/
		// REST Server URL for file upload



		}
}

