<?php
Class Dbconnect{
    public function connect(){
         $host = DB_HOST;
         $user = DB_USER;
         $pass = DB_PASS;
         $db   = DB_NAME;
         $connection = mysqli_connect($host,$user,$pass,$db); 
         return $connection;
     }
}